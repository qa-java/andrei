package packege.lesson;

public class Main {
    public static void main(String[] args) {
        Byte x = Byte.MAX_VALUE;
        Byte x1 = Byte.MIN_VALUE;
        Short x2 = Short.MIN_VALUE;
        Short x3 = Short.MIN_VALUE;
        Long y = Long.MAX_VALUE;
        Long y1 = Long.MIN_VALUE;
        Integer z = Integer.MAX_VALUE;
        Integer z2 = Integer.MIN_VALUE;
        Float y2 = Float.MAX_VALUE;
        Float y3 = Float.MIN_VALUE;
        Double z3 = Double.MIN_VALUE;
        Double z4 = Double.MIN_VALUE;
        Character g = Character.BYTES;

        System.out.println("Byte max: " + x + " ▒ min: " + x1);
        System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
        System.out.println("Short max: " + x2 + " ▒ min: " + x3);
        System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
        System.out.println("Int max: " + z + " ▒ min: " + z2);
        System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
        System.out.println("Long max: " + y + " ▒ min: " + y1);
        System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
        System.out.println("Float max: " + y2 + " ▒ min: " + y3);
        System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
        System.out.println("Double max: " + z3 + " ▒ min: " + z4);

    }
}
